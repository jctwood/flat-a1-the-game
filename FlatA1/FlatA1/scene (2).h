#ifndef SCENE_H
#define SCENE_H

#include <Windows.h>
#include <MMSystem.h>
#include <math.h>
#include "input.h"
#include "vector2D.h"
#include "sprite.h"
#include "animatedSprite.h"
#include "timerclass.h"

#define IDLE 0
#define RUN 1
#define JUMP 2
#define DUCK 3 

class scene
{
public:
	scene();

	void initialise(HWND*, input*);
	void displayFrame();
	void render();
	void resize(HWND*);
	void shutdown();

private:
	void setBuffers();
	bool waitFor(unsigned long);

	HWND *hwnd;		// pointer to the window
	input *currInput;		// pointer to the input class
	HBITMAP theOldFrontBitMap; //,theOldFrontBitMap2;
	RECT screenRect;		
	HDC backHDC, frontHDC;

	// timer
	TimerClass timer;

	//for moving object
	vector2D squarePos;
	vector2D circlePos;
	vector2D bouncePos;
	vector2D bounceVel;
	vector2D marioVel;
	float marioVelMax;
	float marioAcc;
	float marioJump;
	float marioGrav;
	bool marioGrounded;
	
	int bounceSpeed;
	sprite cursorSprite;
	animatedSprite mario;

	int ticker;
};

#endif


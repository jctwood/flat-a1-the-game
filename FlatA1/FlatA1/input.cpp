#include "Input.h"

Input::Input()
{
	for(int i = 0; i < 6; i++)
	{
		mouseButtons[i] = false;
	}
}

void Input::SetKeyDown(WPARAM key)
{
	keys[key] = true;
}

void Input::SetKeyUp(WPARAM key)
{
	keys[key] = false;
}

bool Input::GetKeyDown(WPARAM key)
{
	return keys[key];
}

void Input::SetMouseDown(int button)
{
	mouseButtons[button] = true;
}

void Input::SetMouseUp(int button)
{
	mouseButtons[button] = false;
}

bool Input::GetMouseDown(int button)
{
	return mouseButtons[button];
}

void Input::SetMousePos(Vector2D position)
{
	mousePosition.Set(position);
}

Vector2D Input::GetMousePos()
{
	return mousePosition;
}

bool Input::GetControllerActive(int controller)
{
	DWORD dwResult;
	XINPUT_STATE state;
	ZeroMemory(&state, sizeof(XINPUT_STATE));

	dwResult = XInputGetState(controller, &state);
	
	// Controller is connected 
	if( dwResult == ERROR_SUCCESS )
	{
		return true;
	}

	// Controller is not connected 
	else
	{
		return false;
	}
}

void Input::UpdateControllers()
{
	for (int controller = 0; controller < XUSER_MAX_COUNT; controller++)
	{
		ZeroMemory(&controllers[controller], sizeof(XINPUT_STATE));

		// Get the state
		XInputGetState(controller, &controllers[controller]);
	}
}

bool Input::GetControllerDown(int user, unsigned long button)
{
	if (controllers[user].Gamepad.wButtons & button)
	{
		return true;
	}
	
	else
	{
		return false;
	}
}
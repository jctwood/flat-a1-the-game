#include "Input.h"

void Input::SetKeyDown(WPARAM key)
{
	keys[key] = true;
}

void Input::SetKeyUp(WPARAM key)
{
	keys[key] = false;
}

bool Input::GetKeyDown(WPARAM key)
{
	return keys[key];
}

void Input::SetMouseX(int pos)
{
	mouse.x = pos;
}

void Input::SetMouseY(int pos)
{
	mouse.y = pos;
}

int Input::GetMouseX()
{
	return mouse.x;
}

int Input::GetMouseY()
{
	return mouse.y;
}

//set the mouse booleans - true if pressed
void Input::SetLMouse(bool value)
{
	mouse.left = value;
}

void Input::SetRMouse(bool value)
{
	mouse.right = value;
}

void Input::SetMMouse(bool value)
{
	mouse.middle = value;
}

void Input::SetLDouble(bool value)
{
	mouse.lDouble = value;
}

void Input::SetRDouble(bool value)
{
	mouse.rDouble = value;
}

void Input::SetMDouble(bool value)
{
	mouse.mDouble = value;
}
//Getters for mouse booleans
bool Input::GetLMouse()
{
	return mouse.left;
}
bool Input::GetRMouse()
{
	return mouse.right;
}

bool Input::GetMMouse()
{
	return mouse.middle;
}

bool Input::GetLDouble()
{
	return mouse.lDouble;
}

bool Input::GetRDouble()
{
	return mouse.rDouble;
}

bool Input::GetMDouble()
{
	return mouse.mDouble;
}

void Input::SetLReleased(bool value)
{
	mouse.lReleased = value;
}
void Input::SetRReleased(bool value)
{
	mouse.rReleased = value;
}
void Input::SetMReleased(bool value)
{
	mouse.mReleased = value;
}

bool Input::GetLReleased()
{
	return mouse.lReleased;
}
bool Input::GetRReleased()
{
	return mouse.rReleased;
}
bool Input::GetMReleased()
{
	return mouse.mReleased;
}

void Input::SetMouseDragging(bool value)
{
	mouse.mouseDragging = value;
}

bool Input::GetMouseDragging()
{
	return mouse.mouseDragging;
}


void Input::UpdatePrevMousePos()
{
	mouse.prevX = mouse.x;
	mouse.prevY = mouse.y;
}

int Input::GetPrevMouseX()
{
	return mouse.prevX;
}

int Input::GetPrevMouseY()
{
	return mouse.prevY;
}

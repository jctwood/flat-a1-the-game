#include "Scene.h"

void Scene::Initialise(HWND *lwnd, Input *in)
{
	hwnd = lwnd; //access to the window
	input = in; //access to the user input

	xSpeed = 1;
	ySpeed = 1;

	//initialise all sprites
	gauntlet.Initialise("novelty_gauntlet.bmp", 100, 100);
	face.Initialise("my_sprite.bmp", 500, 100);
	zombie1.Initialise("animZombie.bmp", 1, 0, 55, 108, 100, 0.125);
	zombie2.Initialise("animZombie.bmp", 1, 0, 55, 108, 100, 0.125);
	zombie3.Initialise("animZombie.bmp", 0, 0, 55, 108, 100, 0.125);
	zombie4.Initialise("animZombie.bmp", 0, 0, 55, 108, 100, 0.125);
	//set the zombies speeds and y positions
	zombie1.SetMoveSpeed(0.131);
	zombie1.SetY(zombie1.GetFrameHeight());
	zombie2.SetMoveSpeed(0.146);
	zombie2.SetY(zombie2.GetFrameHeight()*2);
	zombie3.SetMoveSpeed(0.125);
	zombie3.SetY(zombie3.GetFrameHeight()*3);
	zombie4.SetMoveSpeed(0.1);
	zombie4.SetY(zombie4.GetFrameHeight()*4);
	//configure buffers based on window device context
	SetBuffers();
}

void Scene::SetBuffers()
{
	//creates a rect based on window client area
	GetClientRect(*hwnd, &screenRect);
	//initialise the front buffer
	frontHDC = GetDC(*hwnd);
	//set up back DC to be compatible with the front buffer
	backHDC = CreateCompatibleDC(frontHDC);
	//creates bitmap compatible with the front buffer
	prevFrontBitmap = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom);
	prevFlippedBitmap = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom);
	bitmapHDC = CreateCompatibleDC(backHDC);
	flippedHDC = CreateCompatibleDC(backHDC);
	//puts the bitmap onto the back buffer
	SelectObject(backHDC, prevFrontBitmap);
	SelectObject(flippedHDC, prevFlippedBitmap);
	//blank back buffer
	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));
}

void Scene::Render(float eTime)
{
	//user input goes here!
	CheckInput();

	//set the face's position to the arrow keys
	face.SetX(xPos);
	face.SetY(yPos);
	//set the gauntlet position to the mouse
	gauntlet.SetX(x2Pos);
	gauntlet.SetY(y2Pos);

	//render the sprites
	//zombie1.SetY(screenRect.bottom - zombie.GetFrameHeight());
	zombie1.Move(eTime);
	zombie1.Update(eTime);
	zombie1.Draw(bitmapHDC, backHDC);
	zombie2.Move(eTime);
	zombie2.Update(eTime);
	zombie2.Draw(bitmapHDC, backHDC);
	zombie3.Move(eTime);
	zombie3.Update(eTime);
	zombie3.Draw(bitmapHDC, backHDC);
	zombie4.Move(eTime);
	zombie4.Update(eTime);
	zombie4.DrawHFlipped(bitmapHDC, backHDC, flippedHDC);

	gauntlet.Draw(bitmapHDC, backHDC);

	//push to front buffer
	DisplayFrame();
}

void Scene::DisplayFrame()
{
	BitBlt(frontHDC, screenRect.left, screenRect.top, screenRect.right, screenRect.bottom, backHDC, 0, 0, SRCCOPY);

	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));
}

void Scene::Shutdown()
{
	SelectObject(backHDC, prevFrontBitmap);
	DeleteDC(backHDC);
	ReleaseDC(*hwnd,frontHDC);
}

void Scene::CheckInput()
{
	//keyboard
	if (xPos > screenRect.left)
	{
		if(input->GetKeyDown(VK_LEFT))
		{
			xPos--;
		}
	}
	if (xPos <= screenRect.right - 50)
	{
		if(input->GetKeyDown(VK_RIGHT))
		{
			xPos++;
		}
	}
	if (yPos > screenRect.top)
	{
		if(input->GetKeyDown(VK_UP))
		{
			yPos--;
		}
	}
	if (yPos <= screenRect.bottom - 50)
	{
		if(input->GetKeyDown(VK_DOWN))
		{
			yPos++;
		}
	}

	//mouse
	
	x2Pos = input->GetMouseX();
	y2Pos = input->GetMouseY();
}

void Scene::Resize()
{
	SetBuffers();
}
#include "Sprite.h"

void Sprite::SetBitmap(HBITMAP bitmapHandle)
{
	bitmap = bitmapHandle;
}

void Sprite::SetHeight(int heightVal)
{
	height = heightVal;
}

void Sprite::SetWidth(int widthVal)
{
	width = widthVal;
}

void Sprite::SetX(int xVal)
{
	x = xVal;
}

void Sprite::SetY(int yVal)
{
	y = yVal;
}

void Sprite::Initialise(LPSTR filename, int xPos, int yPos)
{
	x = xPos;
	y = yPos;
	LoadBitmap(filename);
	if(!bitmap)
	{
		//failed to load image
		MessageBox(NULL, "Image failed to load", "Error", MB_OK);
	}
}

void Sprite::LoadBitmap(LPSTR szFilename)
{
	bitmap = (HBITMAP)LoadImage(NULL, szFilename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	BITMAP temp;
	GetObject (bitmap, sizeof(temp), &temp);
	width = temp.bmWidth;
	height = temp.bmHeight;
}

void Sprite::Draw(HDC bitmapHDC, HDC backHDC)
{
	HBITMAP originalBitmap;

	originalBitmap = (HBITMAP)SelectObject(bitmapHDC, bitmap);

	//blt image to back buffer
	TransparentBlt(backHDC, x, y, width, height, bitmapHDC, 0, 0, width, height, RGB(255, 0, 255));

	SelectObject(bitmapHDC, originalBitmap);
}

void Sprite::DrawHFlipped(HDC bitmapHDC, HDC backHDC, HDC flippedHDC)
{
	HBITMAP originalBitmap;

	originalBitmap = (HBITMAP)SelectObject(flippedHDC, bitmap);

	//define points
	POINT pt[3];
	pt[0].x = width;
	pt[0].y = 0;
	pt[1].x = 0;
	pt[1].y = 0;
	pt[2].x = width;
	pt[2].y = height;

	//blt image to back buffer
	PlgBlt(flippedHDC, pt, bitmapHDC, 0, 0, width, height, NULL, 0, 0);

	SelectObject(flippedHDC, originalBitmap);

	BitBlt(backHDC, x, y, width, height, flippedHDC, 0, 0, SRCCOPY);
}

void Sprite::DrawStretched(HDC bitmapHDC, HDC backHDC, float stretchAmt)
{
	HBITMAP originalBitmap;

	originalBitmap = (HBITMAP)SelectObject(bitmapHDC, bitmap);

	//blt image to back buffer
	StretchBlt(backHDC, x, y, width * stretchAmt, height * stretchAmt, bitmapHDC, 0, 0, width, height,SRCCOPY);

	SelectObject(bitmapHDC, originalBitmap);
}
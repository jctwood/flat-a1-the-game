//Windows Example Code
//Paul Robertson 2014

// Includes
#include <Windows.h>
#include <stdio.h>
#include "Input.h"
#include <math.h>
#include "Scene.h"
#include "timerclass.h"

// Globals
HWND        hwnd; 
Input input;
Scene scene;
TimerClass timer;
bool programRunning = true;

//Prototypes
LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);
void RegisterMyWindow(HINSTANCE);
BOOL InitialiseMyWindow(HINSTANCE, int);
int WINAPI WinMain (HINSTANCE, HINSTANCE, PSTR, int);

//My Prototypes
void CheckKeyInput();
void CheckMouseInput();
bool ConfirmExit();
void CalculateMouseMove();

// Defines the window
void RegisterMyWindow(HINSTANCE hInstance)
{
    WNDCLASSEX  wcex;									

    wcex.cbSize        = sizeof (wcex);				
    wcex.style         = CS_HREDRAW | CS_VREDRAW;		
    wcex.lpfnWndProc   = WndProc;						
    wcex.cbClsExtra    = 0;								
    wcex.cbWndExtra    = 0;								
    wcex.hInstance     = hInstance;						
    wcex.hIcon         = 0; 
    wcex.hCursor       = LoadCursor (NULL, IDC_ARROW);													
    wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW+1);
    wcex.lpszMenuName  = NULL;							
    wcex.lpszClassName = "FirstWindowClass";				
    wcex.hIconSm       = 0; 

	RegisterClassEx (&wcex);							
}

// Attempts to create the window and display it
BOOL InitialiseMyWindow(HINSTANCE hInstance, int nCmdShow)
{
  
   hwnd = CreateWindow ("FirstWindowClass",					
						 "A Window",		  	
						 WS_CAPTION|WS_SYSMENU|WS_THICKFRAME,
						 50,	//Position x		
						 50,	//Position y
						 800,	//Size x
						 600,	//Size y
						 NULL,					
						 NULL,					
						 hInstance,				
						 NULL);							
	if (!hwnd)
	{
		return FALSE;
	}

    ShowWindow (hwnd, nCmdShow);						
    UpdateWindow (hwnd);									
	return TRUE;

}

// Entry point. The program start here
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int nCmdShow)
{									
    MSG         msg;								
	RegisterMyWindow(hInstance);

   	if (!InitialiseMyWindow(hInstance, nCmdShow))
		return FALSE;

	scene.Initialise(&hwnd, &input);
	ShowCursor(FALSE);
	timer.Initialise();
	while (programRunning)					
    {							
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
		    if (msg.message==WM_QUIT)
				break;
			TranslateMessage (&msg);							
			DispatchMessage (&msg);
		}

		else
		{
			timer.Frame();
			scene.Render(timer.GetTime());
		}
    }
	scene.Shutdown();
	return msg.wParam ;										
}

// handles window messages				
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)											
    {
		case WM_KEYDOWN:
			input.SetKeyDown(wParam);
			break;

		case WM_KEYUP:
			input.SetKeyUp(wParam);
			break;

		case WM_MOUSEMOVE:
			input.SetMouseX(LOWORD(lParam));
			input.SetMouseY(HIWORD(lParam));
			break;
		
		case WM_LBUTTONDOWN:
			input.SetLMouse(true);
			break;

		case WM_RBUTTONDOWN:
			input.SetRMouse(true);
			break;

		case WM_MBUTTONDOWN:
			input.SetMMouse(true);
			break;

		case WM_LBUTTONUP:
			input.SetLReleased(true);
			input.SetLMouse(false);
			break;

		case WM_RBUTTONUP:
			input.SetRReleased(true);
			input.SetRMouse(false);
			break;

		case WM_MBUTTONUP:
			input.SetMReleased(true);
			input.SetMMouse(false);
			break;

		case WM_SIZING:
			scene.Resize();
			break;
		case WM_CLOSE:
			programRunning = ConfirmExit();
			break;

		case WM_DESTROY:	
			PostQuitMessage(0);	
			break;	
	}													

	return DefWindowProc (hwnd, message, wParam, lParam);		
															
}


void CheckKeyInput()
{
	if (input.GetKeyDown(VK_SPACE))
	{
		MessageBox(NULL, "Space Was Pressed", "Key Info", MB_OK);
		input.SetKeyUp(VK_SPACE);
	}
	if (input.GetKeyDown('W'))
	{
		MessageBox(NULL, "W Was Pressed", "Key Info", MB_OK);
		input.SetKeyUp('W');
	}
	if(input.GetKeyDown('J') && input.GetKeyDown('K') && input.GetKeyDown('L'))
	{
		MessageBox(NULL, "J, K and L Were Pressed", "Key Info", MB_OK);
		input.SetKeyUp('J');
		input.SetKeyUp('K');
		input.SetKeyUp('L');
	}
	if(input.GetKeyDown(VK_ESCAPE))
	{
		programRunning = ConfirmExit();
		input.SetKeyUp(VK_ESCAPE);
	}
}

void CheckMouseInput()
{
	if (input.GetLMouse() && !input.GetMouseDragging())
	{
		input.SetMouseDragging(true);
		input.UpdatePrevMousePos();
	}

	if(input.GetLReleased())
	{
		CalculateMouseMove();
	}
}

bool ConfirmExit()
{
	ShowCursor(TRUE);
	int resp = MessageBox(hwnd, "Are You Sure?","Quit", MB_YESNO);
	if (resp == IDYES)
	{
		return false;
	}
	return true;
}

void CalculateMouseMove()
{
	int dX = input.GetMouseX() - input.GetPrevMouseX();
	int dY = input.GetMouseY() - input.GetPrevMouseY();

	double distance = sqrt((double(dX) * double(dX)) + (double(dY) * double(dY)));
	char output[50];
	sprintf(output, "The mouse moved %d pixels", int(distance));
	MessageBox(hwnd, output, "Mouse Info", MB_OK);
	
	input.SetLReleased(false);
	input.SetMouseDragging(false);
}
#include "Animated_Sprite.h"

void AnimatedSprite::Initialise(LPSTR filename, int xPos, int yPos, int spriteWidth, int spriteHeight, int animSpeed, float movementSpeed)
{
	x = xPos;
	y = yPos;
	frameWidth = spriteWidth;
	frameHeight = spriteHeight;
	animationSpeed = animSpeed;
	moveSpeed = movementSpeed;
	offsetX = 0;
	offsetY = 0;
	LoadBitmap(filename);
	if(!bitmap)
	{
		//failed to load image
		MessageBox(NULL, "Image failed to load", "Error", MB_OK);
	}
}

void AnimatedSprite::Update(float elapsedTime)
{
	animTime += elapsedTime;
	if(animTime >= animationSpeed)
	{
		offsetX += frameWidth;
		if(offsetX > width - frameWidth)
		{
			offsetX = 0;
		}
		animTime = 0;
	}
}

void AnimatedSprite::Move(float elapsedTime)
{
	moveTime += ((float)moveSpeed * elapsedTime);
	if (moveTime > 1)
	{
		x++;
		moveTime = 0;
	}
}

void AnimatedSprite::Draw(HDC bitmapHDC, HDC backHDC)
{
	HBITMAP originalBitmap;

	originalBitmap = (HBITMAP)SelectObject(bitmapHDC, bitmap);

	//blt image to back buffer
	TransparentBlt(backHDC, x, y, frameWidth, frameHeight, bitmapHDC, offsetX, offsetY, frameWidth, frameHeight, RGB(0, 128, 128));

	SelectObject(bitmapHDC, originalBitmap);
}

void AnimatedSprite::DrawHFlipped(HDC bitmapHDC, HDC backHDC, HDC flippedHDC)
{
	HBITMAP originalBitmap;
	HBITMAP flippedBitmap;

	originalBitmap = (HBITMAP)SelectObject(bitmapHDC, bitmap);
	flippedBitmap = (HBITMAP)SelectObject(flippedHDC, bitmap);
	
	SelectObject(flippedHDC, flippedBitmap);
	
	//define points
	POINT pt[3];
	pt[0].x = frameWidth-1;
	pt[0].y = 0;
	pt[1].x = -1;
	pt[1].y = 0;
	pt[2].x = frameWidth-1;
	pt[2].y = frameHeight;

	PlgBlt(flippedHDC, pt, bitmapHDC, offsetX, offsetY, frameWidth, frameHeight, NULL, 0, 0);

	//blt image to back buffer
	TransparentBlt(backHDC, x, y, frameWidth, frameHeight, flippedHDC, 0, 0, frameWidth, frameHeight, RGB(0, 128, 128));

	SelectObject(bitmapHDC, originalBitmap);	
}
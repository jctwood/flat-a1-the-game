#ifndef SPRITE_H
#define SPRITE_H

#include <Windows.h>
#include <MMSystem.h>
#include <stdio.h>

class Sprite
{
public:
	//handles loding image and initialising
	void Initialise(LPSTR, int, int);
	void SetX(int);
	void SetY(int);
	void SetWidth(int);
	void SetHeight(int);
	void SetBitmap(HBITMAP);	
	void Draw(HDC bitmapHDC, HDC backHDC);
	void DrawHFlipped(HDC bitmapHDC, HDC backHDC, HDC flippedHDC);
	void DrawStretched(HDC bitmapHDC, HDC backHDC, float scaleAmt);

	inline int GetX() {return x;}
	inline int GetY() {return y;}
	inline int GetWidth() {return  width;}
	inline int GetHeight() {return height;}
	inline HBITMAP GetBitmap() {return bitmap;}

protected:
	void LoadBitmap(LPSTR);
	int x, y, width, height;
	HBITMAP bitmap;
};

#endif
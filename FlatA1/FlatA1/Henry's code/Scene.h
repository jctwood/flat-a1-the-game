#ifndef	SCENE_H
#define SCENE_H

#include <Windows.h>
#include <MMSystem.h>
#include "Input.h"
#include <math.h>
#include <ctime>
#include "Sprite.h"
#include "Animated_Sprite.h"

class Scene
{

public:
	void Initialise(HWND*, Input*);
	void DisplayFrame();
	void Render(float);
	void Resize();
	void Shutdown();

private:
	void SetBuffers();
	BOOL WaitFor(unsigned long);
	void CheckInput();

	HWND *hwnd;	//pointer to the window
	Input *input;	//pointer to the input class
	HBITMAP prevFrontBitmap, prevFlippedBitmap;
	RECT screenRect;
	HDC backHDC, frontHDC, bitmapHDC, flippedHDC; //buffers

	//for moving object
	unsigned int xPos, yPos, x2Pos, y2Pos;
	int xSpeed, ySpeed;

	Sprite gauntlet, face;
	AnimatedSprite zombie1, zombie2, zombie3, zombie4;
};

#endif
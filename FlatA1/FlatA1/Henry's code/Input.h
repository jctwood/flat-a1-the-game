#ifndef INPUT_H
#define INPUT_H

#include<Windows.h>


class Input
{
	typedef struct Mouse
	{
		int x, y, prevX, prevY;
		bool left, right, middle, lDouble, rDouble, mDouble, lReleased, rReleased, mReleased;
		bool mouseDragging;
	};
public:
	void SetKeyDown(WPARAM);
	void SetKeyUp(WPARAM);

	bool GetKeyDown(WPARAM);
	void SetMouseX(int);
	void SetMouseY(int);
	int GetMouseX();
	int GetMouseY();
	void SetLMouse(bool);
	void SetRMouse(bool);
	void SetMMouse(bool);
	void SetLDouble(bool);
	void SetRDouble(bool);
	void SetMDouble(bool);
	void SetLReleased(bool);
	void SetRReleased(bool);
	void SetMReleased(bool);
	bool GetLReleased();
	bool GetRReleased();
	bool GetMReleased();
	bool GetLMouse();
	bool GetRMouse();
	bool GetMMouse();
	bool GetLDouble();
	bool GetRDouble();
	bool GetMDouble();
	void SetMouseDragging(bool);
	bool GetMouseDragging();

	void UpdatePrevMousePos();
	int GetPrevMouseX();
	int GetPrevMouseY();

private:
	bool keys[256];
	Mouse mouse;
};

#endif
#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include "Sprite.h"

class AnimatedSprite: public Sprite
{
public:
	void Initialise(LPSTR, int, int, int, int, int, float);
	void Update(float);
	void Move(float);
	void Draw(HDC bitmapHDC, HDC backHDC);
	void DrawHFlipped(HDC bitmapHDC, HDC backHDC, HDC flippedHDC);

	inline int GetAnimSpeed() {return animationSpeed;}
	inline int GetMoveSpeed() {return moveSpeed;}
	inline int GetFrameWidth() {return frameWidth;}
	inline int GetFrameHeight() {return frameHeight;}
	//inline float GetAnimTime() {return animTime;}
	//inline float GetMoveTime() {return moveTime;}
	//inline int GetOffsetY() {return offsetY;}
	//inline int GetOffsetX() {return offsetX;}
	inline void SetAnimSpeed(int val) {animationSpeed = val;}
	inline void SetMoveSpeed(float val) {moveSpeed = val;}
	inline void SetFrameWidth(int val) {frameWidth = val;}
	inline void SetFrameHeight(int val) {frameHeight = val;}

protected:
	int animationSpeed;
	float moveSpeed;
	int frameWidth;
	int frameHeight;

	float animTime;
	float moveTime;
	int offsetX;
	int offsetY;
};

#endif
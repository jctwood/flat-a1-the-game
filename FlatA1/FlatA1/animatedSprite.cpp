#include "AnimatedSprite.h"

AnimatedSprite::AnimatedSprite()
{
	
}

void AnimatedSprite::Initialise(LPSTR file, int xPos, int yPos, int width, int height, float animationSpeed, HDC *back, HDC *bitmap, HDC *flippedBitmap)
{
	// Load bitmap
	x = xPos;
	y = yPos;
	frameWidth = width;
	frameHeight = height;
	animSpeed = animationSpeed;
	currFrame = 0;
	
	bitmapHDC = bitmap;
	backHDC = back;
	flippedHDC = flippedBitmap;

	LoadBitmap(file);
	flipped = false;

	// Display error message
	if (!bitmap)
	{
		MessageBox(NULL, "Image did not load", "Error", MB_OK);
	}
}

void AnimatedSprite::Animate(float dTime)
{
	eTime += dTime;

	// Check if enough time has elapsed to advance anim
	if (eTime > animSpeed)
	{
		currFrame++;

		// If the last sprite was drawn restart animation
		if ((currFrame + 1) > (width / frameWidth))
		{
			currFrame = 0;
		}

		eTime = 0;
	}
}

void AnimatedSprite::SetAnimation(int animation)
{
	currAnim = animation;
}

void AnimatedSprite::DrawFrame()
{
	HBITMAP originalBitmap, originalFlippedBitmap;
	
	originalBitmap = (HBITMAP)SelectObject(*bitmapHDC, bitmap);
	originalFlippedBitmap = (HBITMAP)SelectObject(*flippedHDC, bitmap);

    SelectObject(*flippedHDC, originalFlippedBitmap); 
	
	// Array to flip sprite
	POINT pt[3];
	pt[0].x = frameWidth - 1;
	pt[0].y = 0;
	pt[1].x = -1;
	pt[1].y = 0;
	pt[2].x = frameWidth - 1;
	pt[2].y = frameHeight;

	// Flip then copy to buffer
	if (flipped)
	{
		PlgBlt(*flippedHDC, pt, *bitmapHDC, currFrame * frameWidth, currAnim * frameHeight, frameWidth, frameHeight, NULL, 0, 0);
		TransparentBlt(*backHDC, x, y, frameWidth, frameHeight, *flippedHDC, 0, 0, frameWidth, frameHeight, RGB(255, 0, 255));
	}

	// Copy to buffer
	else
	{
		TransparentBlt(*backHDC, x, y, frameWidth, frameHeight, *bitmapHDC, currFrame * frameWidth, currAnim * frameHeight, frameWidth, frameHeight, RGB(255, 0, 255));
	}
	
	SelectObject(*bitmapHDC, originalBitmap);
}

void AnimatedSprite::DrawSprite()
{
	HBITMAP originalBitmap, originalFlippedBitmap;
	
	originalBitmap = (HBITMAP)SelectObject(*bitmapHDC, bitmap);
	originalFlippedBitmap = (HBITMAP)SelectObject(*flippedHDC, bitmap);

    SelectObject(*flippedHDC, originalFlippedBitmap); 
	
	// Array to flip sprite
	POINT pt[3];
	pt[0].x = frameWidth - 1;
	pt[0].y = 0;
	pt[1].x = -1;
	pt[1].y = 0;
	pt[2].x = frameWidth - 1;
	pt[2].y = frameHeight;

	// Flip then copy to buffer
	if (flipped)
	{
		PlgBlt(*flippedHDC, pt, *bitmapHDC, 0, 0, frameWidth, frameHeight, NULL, 0, 0);
		TransparentBlt(*backHDC, x, y, frameWidth, frameHeight, *flippedHDC, 0, 0, frameWidth, frameHeight, RGB(255, 0, 255));
	}

	// Copy to buffer
	else
	{
		TransparentBlt(*backHDC, x, y, frameWidth, frameHeight, *bitmapHDC, 0, 0, frameWidth, frameHeight, RGB(255, 0, 255));
	}
	
	SelectObject(*bitmapHDC, originalBitmap);
}

int AnimatedSprite::GetHeight()
{
	return frameHeight;
}

int AnimatedSprite::GetWidth()
{
	return frameWidth;
}
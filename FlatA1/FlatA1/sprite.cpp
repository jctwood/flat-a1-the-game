#include "Sprite.h"

Sprite::Sprite()
{
	
}

void Sprite::Initialise(LPSTR filename, int xPos, int yPos, HDC *back, HDC *bitmap, HDC *flippedBitmap)
{
	// Load bitmap
	x = xPos;
	y = yPos;
	LoadBitmap(filename);
	flipped = false;
	
	bitmapHDC = bitmap;
	backHDC = back;
	flippedHDC = flippedBitmap;

	// Display error message
	if (!bitmap)
	{
		MessageBox(NULL, "Image did not load", "Error", MB_OK);
	}
}

void Sprite::LoadBitmap(LPSTR fileName)
{
	bitmap = (HBITMAP)LoadImage(NULL, fileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	BITMAP temp;
	GetObject(bitmap, sizeof(temp), &temp);

	width = temp.bmWidth;
	height = temp.bmHeight;
}

void Sprite::DrawSprite()
{
	HBITMAP originalBitmap, originalFlippedBitmap;
	
	originalBitmap = (HBITMAP)SelectObject(*bitmapHDC, bitmap);
	originalFlippedBitmap = (HBITMAP)SelectObject(*flippedHDC, bitmap);

    SelectObject(*flippedHDC, originalFlippedBitmap); 
	
	// Array to flip sprite
	POINT pt[3];
	pt[0].x = width - 1;
	pt[0].y = 0;
	pt[1].x = -1;
	pt[1].y = 0;
	pt[2].x = width - 1;
	pt[2].y = height;

	// Flip then copy to buffer
	if (flipped)
	{
		PlgBlt(*flippedHDC, pt, *bitmapHDC, 0, 0, width, height, NULL, 0, 0);
		TransparentBlt(*backHDC, x, y, width, height, *flippedHDC, 0, 0, width, height, RGB(255, 0, 255));
	}

	// Copy to buffer
	else
	{
		TransparentBlt(*backHDC, x, y, width, height, *bitmapHDC, 0, 0, width, height, RGB(255, 0, 255));
	}
	
	SelectObject(*bitmapHDC, originalBitmap);
}

void Sprite::SetX(int xPos)
{
	x = xPos;
}

void Sprite::SetY(int yPos)
{
	y = yPos;
}

void Sprite::SetWidth(int newWidth)
{
	width = newWidth;
}

void Sprite::SetHeight(int newHeight)
{
	height = newHeight;
}

int Sprite::GetX()
{
	return x;
}

int Sprite::GetY()
{
	return y;
}

int Sprite::GetHeight()
{
	return height;
}

int Sprite::GetWidth()
{
	return width;
}

HBITMAP Sprite::GetBitmap()
{
	return bitmap;
}

void Sprite::Flip()
{
	flipped = !flipped;
}

bool Sprite::GetFlipped()
{
	return flipped;
}
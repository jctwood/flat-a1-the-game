#ifndef INPUT_H
#define INPUT_H

#include <windows.h>
#include <stdio.h>
#include <XInput.h>
#include "Vector2D.h"

#define XUSER_MAX_COUNT 4

class Input
{
public:
		Input();
		void SetMouseDown(int);
		void SetMouseUp(int);
		bool GetMouseDown(int);
		void SetMousePos(Vector2D);
		Vector2D GetMousePos();
		void SetKeyDown(WPARAM);
		void SetKeyUp(WPARAM);
		bool GetKeyDown(WPARAM);
		bool GetControllerActive(int);
		bool GetControllerDown(int, unsigned long);
		void UpdateControllers();

private:
		bool keys[256];
		bool mouseButtons[6];
		Vector2D mousePosition;
		XINPUT_STATE controllers[XUSER_MAX_COUNT];
};

#endif
#ifndef PLAYER_H
#define PLAYER_H

class Player:public GameObject
{
public:
	// methods
	Player();

	void Update(); // deals with input and moves gameobject

protected:
	// members
	int playerNumber;
};

#endif
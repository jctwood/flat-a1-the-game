// Includes
#include <windows.h>
#include <stdio.h>
#include <string.h>

#include "Scene.h"
#include "Input.h"
#include "Vector2D.h"
#include "Timer.h"

#define LEFT 0
#define RIGHT 1
#define MIDDLE 2

// Globals
HWND hwnd; 

Scene newScene;

Timer timer;

//Prototypes
LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);
void RegisterMyWindow(HINSTANCE);
BOOL InitialiseMyWindow(HINSTANCE, int, int, int, int, int);
int WINAPI WinMain (HINSTANCE, HINSTANCE, PSTR, int);
Input currInput;

// Defines the window
void RegisterMyWindow(HINSTANCE hInstance)
{
    WNDCLASSEX  wcex;									

    wcex.cbSize        = sizeof (wcex);				
    wcex.style         = CS_HREDRAW | CS_VREDRAW;		
    wcex.lpfnWndProc   = WndProc;						
    wcex.cbClsExtra    = 0;								
    wcex.cbWndExtra    = 0;								
    wcex.hInstance     = hInstance;						
    wcex.hIcon         = 0; 
    wcex.hCursor       = LoadCursor (NULL, IDC_ARROW);													
    wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
    wcex.lpszMenuName  = NULL;							
    wcex.lpszClassName = "WindowController";				
    wcex.hIconSm       = 0; 

	RegisterClassEx (&wcex);						
}

// Attempts to create the window and display it
BOOL InitialiseMyWindow(HINSTANCE hInstance, int nCmdShow, int x, int y, int width, int height)
{
   hwnd = CreateWindow ("WindowController",					
						 "Flat A1: The Game",		  	
						 WS_SYSMENU|WS_THICKFRAME,
						 x,
						 y,			
						 width,			
						 height,			
						 NULL,					
						 NULL,					
						 hInstance,				
						 NULL);								
	if (!hwnd)
	{
		return FALSE;
	}

    ShowWindow (hwnd, nCmdShow);						
    UpdateWindow (hwnd);									
	return TRUE;
}

// Entry point. The program start here
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int nCmdShow)
{									
    MSG msg;								
	RegisterMyWindow(hInstance);
	
	int screen_width = GetSystemMetrics(SM_CXFULLSCREEN);
	int screen_height = GetSystemMetrics(SM_CYFULLSCREEN);
	int window_width = 400;
	int window_height = 400;

   	if (!InitialiseMyWindow(hInstance, nCmdShow, (screen_width / 2) - (window_width / 2), (screen_height / 2) - (window_height / 2), window_width, window_height))
	{
		return FALSE;
	}
	
	timer.Initialise();

	newScene.Initialise(&hwnd, &currInput);

	while (TRUE)					
    {			
		timer.Frame();
		
		currInput.UpdateControllers();

		if (currInput.GetKeyDown('C'))
		{
			if (currInput.GetControllerActive(0))
			{
				MessageBox(NULL, "Controller 1 IS connected", "Controller status", MB_OK);
			}

			else
			{
				MessageBox(NULL, "Controller 1 NOT connected", "Controller status", MB_OK);
			}
		}

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
		    if (msg.message == WM_QUIT)
			{
				break;
			}

			TranslateMessage (&msg);							
			DispatchMessage (&msg);
		}

		else
		{		
			newScene.Update(timer.GetTime());	// call render on scene object
			newScene.Render(timer.GetTime());

			// quit if user presses escape
			if (currInput.GetKeyDown(VK_ESCAPE))
			{
				PostQuitMessage(WM_QUIT);
			}			
		}
    }

	newScene.Shutdown();
    
	return msg.wParam ;										
}

// handles window messages				
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)											
    {														
		case WM_CREATE:											
			break;

		case WM_SIZE:
				newScene.Resize(&hwnd);
			break;	

		case WM_KEYDOWN:
				currInput.SetKeyDown(wParam);
			break;

		case WM_KEYUP:
				currInput.SetKeyUp(wParam);
			break;

		case WM_LBUTTONDOWN:
				currInput.SetMouseDown(LEFT);
			break;

		case WM_LBUTTONUP:
				currInput.SetMouseUp(LEFT);
			break;

		case WM_RBUTTONDOWN:
				currInput.SetMouseDown(RIGHT);
			break;

		case WM_RBUTTONUP:
				currInput.SetMouseUp(RIGHT);
			break;

		case WM_MBUTTONDOWN:
				currInput.SetMouseDown(MIDDLE);
			break;
			
		case WM_MBUTTONUP:
				currInput.SetMouseUp(MIDDLE);
			break;

		case WM_MOUSEMOVE:   
				currInput.SetMousePos(Vector2D(LOWORD (lParam), HIWORD (lParam)));
			break;

		case WM_PAINT:
		    break;		

		case WM_DESTROY:	
				PostQuitMessage(0);
			break;		

		case WM_CLOSE:
				PostQuitMessage(0);
			break;
		case WM_QUIT:
				PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hwnd, message, wParam, lParam);
	}													

	return DefWindowProc (hwnd, message, wParam, lParam);															
}
#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "Vector2D.h"
#include "AnimatedSprite.h"

class GameObject:public AnimatedSprite
{
public:
	// methods
	GameObject();

	void Move();

	void SetGridPos(vector2D newPos);
	void SetGridPos(float newX, float newY);
	void SetGridX(float newX);
	void SetGridY(float newY);
	void SetGridHeight(float newHeight);

	inline void SetVel(float newX, float newY){ velocity.set(newX, newY); }
	inline void SetVel(vector2D newVel){ velocity.set(newVel); }
	inline void SetVelX(float newX){ velocity.x = newX; }
	inline void SetVelY(float newY){ velocity.y = newY; }

	inline void SetAcc(float newX, float newY){ velocity.set(newX, newY); }
	inline void SetAcc(vector2D newAcc){ acceleration.set(newAcc); }
	inline void SetAccX(float newX){ acceleration.x = newX; }
	inline void SetAccY(float newY){ acceleration.y = newY; }

protected:
	// members
	float gridX, gridY, height;
	vector2D velocity;
	vector2D acceleration;
};

#endif
#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <math.h>

class Vector2D
{
public: float x;
		float y;
		
		Vector2D();
		Vector2D(float, float);
		float Magnitude();
		void Add(Vector2D);
		float Trajectory();
		void Set(float, float);
		void Set(Vector2D);
};

#endif
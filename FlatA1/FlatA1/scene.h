#ifndef SCENE_H
#define SCENE_H

#include <Windows.h>
#include <MMSystem.h>
#include <math.h>
#include "Input.h"
#include "Vector2D.h"
#include "Sprite.h"
#include "AnimatedSprite.h"

#define IDLE 0
#define RUN 1
#define JUMP 2
#define DUCK 3

class Scene
{
public:
	Scene();

	void Initialise(HWND*, Input*);
	void DisplayFrame();
	void Update(float dTime);
	void Render(float dTime);
	void Resize(HWND*);
	void Shutdown();

private:
	void SetBuffers();

	HWND *hwnd;		// pointer to the window
	Input *currInput;		// pointer to the input class
	HBITMAP theOldFrontBitMap, originalFlippedBitMap; //,theOldFrontBitMap2;
	RECT screenRect;		
	HDC frontHDC, backHDC, flippedHDC, bitmapHDC;

	// Mario
	Vector2D marioVel;
	float marioVelMax;
	float marioAcc;
	float marioJump;
	float marioGrav;
	bool marioGrounded;
	
	AnimatedSprite mario;
};

#endif


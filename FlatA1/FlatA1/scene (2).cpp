#include "scene.h"

scene::scene()
{
	
}

void scene::initialise(HWND* lwnd, input* in)
{
	hwnd = lwnd;	// access to the window
	currInput = in;	// access to user input

	ticker = 0;	// used for moving object

	timer.Initialize();

	bounceSpeed = 5;
	bounceVel.set(bounceSpeed, bounceSpeed);
	bouncePos.set(0, 0);

	marioVel.set(0, 0);
	marioJump = -15;
	marioVelMax = 4;
	marioAcc = 1;
	marioGrav = 2;
	marioGrounded = true;

	cursorSprite.initialise(&backHDC, "transicon.bmp", 100, 100);
	mario.initialise(&backHDC, "marioAnim.bmp", 100, screenRect.bottom - 23, 16, 23, 100);
	//mario.initialise(&backHDC, "mario.bmp", 100, screenRect.bottom - 20);
	
	// configure buffers, based on window device context
	setBuffers();
}

void scene::setBuffers()
{
	// creates rect based on window client area
    GetClientRect(*hwnd, &screenRect); 

    // Initialises front buffer device context (window)
    frontHDC = GetDC(*hwnd);

    // sets up Back DC to be compatible with the front
    backHDC = CreateCompatibleDC(frontHDC);
	//temphdc = CreateCompatibleDC(frontHDC);

    //creates bitmap compatible with the front buffer
    theOldFrontBitMap = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom); 
	//theOldFrontBitMap2 = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom); 
   
	// puts the bitmap onto the back buffer
    SelectObject(backHDC, theOldFrontBitMap); 
	//SelectObject(temphdc, theOldFrontBitMap2); 
   
	// blank back buffer
    FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));
	//FillRect(temphdc, &screenRect, (HBRUSH)GetStockObject(0));
}

void scene::displayFrame()
{
	
	//BitBlt(temphdc, screenRect.left,screenRect.top, screenRect.right, screenRect.bottom, frontHDC , 0, 0, SRCCOPY);
	//BitBlt(temphdc, screenRect.left,screenRect.top, screenRect.right, screenRect.bottom, backHDC, 0, 0, SRCINVERT);
   // BitBlt(frontHDC, screenRect.left,screenRect.top, screenRect.right, screenRect.bottom, temphdc, 0, 0, NOTSRCCOPY);
	BitBlt(frontHDC, screenRect.left,screenRect.top, screenRect.right, screenRect.bottom, backHDC, 0, 0, SRCCOPY);

	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));
}

void scene::render()
{
	timer.Frame();

	if (waitFor(5))
	{
		cursorSprite.setX(currInput->getMousePos().x);
		cursorSprite.setY(currInput->getMousePos().y);

		if (currInput->getKeyDown(VK_RIGHT))
		{
			if (marioVel.x < marioVelMax)
			{
				marioVel.x += marioAcc;
			}

			if (mario.getFlipped())
			{
				mario.flip();
			}

			mario.setAnimation(RUN);
		}

		else if (currInput->getKeyDown(VK_LEFT))
		{
			if (abs(marioVel.x) < marioVelMax)
			{
				marioVel.x -= marioAcc;
			}

			if (!mario.getFlipped())
			{
				mario.flip();
			}
		
			mario.setAnimation(RUN);
		}

		else
		{
			if (marioVel.x < 0)
			{
				marioVel.x += marioAcc;

				if (marioVel.x > 0)
				{
					marioVel.x = 0;
				}
			}

			else if (marioVel.x > 0)
			{
				marioVel.x -= marioAcc;

				if (marioVel.x < 0)
				{
					marioVel.x = 0;
				}
			}
		
			mario.setAnimation(IDLE);
		}
		
		mario.setX(mario.getX() + marioVel.x);

		if (currInput->getKeyDown(VK_UP) && marioGrounded)
		{
			//squarePos.y--;
			marioVel.y = marioJump;
			marioGrounded = false;
		
			mario.setAnimation(JUMP);
		}

		else if (!marioGrounded)
		{
			if (mario.getY() >= (screenRect.bottom - mario.getHeight()))
			{
				marioVel.y = 0;
				mario.setY(screenRect.bottom - mario.getHeight());
				marioGrounded = true;
		
				mario.setAnimation(IDLE);
			}

			else
			{
				marioVel.y += marioGrav;
		
				mario.setAnimation(JUMP);
			}
		}

		mario.setY(mario.getY() + marioVel.y);

		if (currInput->getKeyDown(VK_DOWN))
		{
			mario.setAnimation(DUCK);
		}
	}

	mario.animate(timer.GetTime());

	mario.drawFrame();

	//finally push to front buffer (screen)
	displayFrame();
}

void scene::resize(HWND* newHandler)
{
    hwnd = newHandler; 

	setBuffers();
}

void scene::shutdown()
{
    SelectObject(backHDC,theOldFrontBitMap);
    DeleteDC(backHDC);
    ReleaseDC(*hwnd,frontHDC);
}

bool scene::waitFor(unsigned long delay)
{
	static unsigned long clockStart = 0;

	unsigned long timePassed;
	unsigned long now = timeGetTime();

	timePassed = now - clockStart;

	if (timePassed > delay)
	{
		clockStart = now;
		return true;
	}

	else
	{
		return false;
	}
}
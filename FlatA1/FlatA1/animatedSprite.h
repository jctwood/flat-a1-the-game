#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include <Windows.h>
#include <MMSystem.h>
#include <stdio.h>
#include "sprite.h"

class AnimatedSprite : public Sprite
{
public:
	AnimatedSprite();
	void Initialise(LPSTR file, int xPos, int yPos, int width, int height, float animationSpeed, HDC *back, HDC *bitmap, HDC *flippedBitmap);
	void Animate(float);
	void SetAnimation(int);
	void DrawFrame();
	void DrawSprite();
	int GetHeight();
	int GetWidth();

private:
	float eTime;
	float animSpeed;
	int currFrame;
	int currAnim;
	int frameWidth;
	int frameHeight;
};

#endif

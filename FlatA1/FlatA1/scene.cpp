#include "Scene.h"

Scene::Scene()
{
	
}

void Scene::Initialise(HWND* lwnd, Input* in)
{
	hwnd = lwnd;	// access to the window
	currInput = in;	// access to user input

	marioVel.Set(0, 0);
	marioJump = -10.0;
	marioVelMax = 1.0;
	marioAcc = 0.1;
	marioGrav = 1.5;
	marioGrounded = true;

	mario.Initialise("marioAnim.bmp", 100, screenRect.bottom - 23, 16, 23, 250, &backHDC, &bitmapHDC, &flippedHDC);
	
	// configure buffers, based on window device context
	SetBuffers();
}

void Scene::SetBuffers()
{
	// creates rect based on window client area
    GetClientRect(*hwnd, &screenRect); 

    // Initialises front buffer device context (window)
    frontHDC = GetDC(*hwnd);

    // sets up Back DC to be compatible with the front
    backHDC = CreateCompatibleDC(frontHDC);

    //creates bitmap compatible with the front buffer
    theOldFrontBitMap = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom); 
    originalFlippedBitMap = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom); 
	
    flippedHDC = CreateCompatibleDC(backHDC);
	bitmapHDC = CreateCompatibleDC(backHDC);

	// puts the bitmap onto the back buffer
    SelectObject(backHDC, theOldFrontBitMap);
	SelectObject(flippedHDC, originalFlippedBitMap);
	
	// blank back buffer
    FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));
}

void Scene::DisplayFrame()
{
	BitBlt(frontHDC, screenRect.left,screenRect.top, screenRect.right, screenRect.bottom, backHDC, 0, 0, SRCCOPY);

	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));
}

void Scene::Update(float dTime)
{

	if (currInput->GetKeyDown(VK_RIGHT) || currInput->GetControllerDown(0, XINPUT_GAMEPAD_DPAD_RIGHT))
	{
		if (marioVel.x < marioVelMax)
		{
			marioVel.x += marioAcc * dTime;
		}

		if (mario.GetFlipped())
		{
			mario.Flip();
		}

		mario.SetAnimation(RUN);
	}

	else if (currInput->GetKeyDown(VK_LEFT) || currInput->GetControllerDown(0, XINPUT_GAMEPAD_DPAD_LEFT))
	{
		if (marioVel.x > -marioVelMax)
		{
			marioVel.x -= marioAcc * dTime;
		}

		if (!mario.GetFlipped())
		{
			mario.Flip();
		}

		mario.SetAnimation(RUN);
	}

	else
	{
		if (marioVel.x < 0)
		{
			marioVel.x += marioAcc * dTime;

			if (marioVel.x > 0)
			{
				marioVel.x = 0;
			}
		}

		else if (marioVel.x > 0)
		{
			marioVel.x -= marioAcc * dTime;

			if (marioVel.x < 0)
			{
				marioVel.x = 0;
			}
		}

		mario.SetAnimation(IDLE);
	}

	if (marioGrounded && (currInput->GetKeyDown(VK_UP) || currInput->GetControllerDown(0, XINPUT_GAMEPAD_DPAD_UP)))
	{
		marioVel.y = marioJump;
		marioGrounded = false;

		mario.SetAnimation(JUMP);
	}

	else if (!marioGrounded)
	{
		if (mario.GetY() >= (screenRect.bottom - mario.GetHeight()))
		{
			marioVel.y = 0;
			mario.SetY(screenRect.bottom - mario.GetHeight());
			marioGrounded = true;

			mario.SetAnimation(IDLE);
		}

		else
		{
			marioVel.y += marioGrav * dTime;

			mario.SetAnimation(JUMP);
		}
	}

	mario.SetY(mario.GetY() + (marioVel.y * dTime));
	mario.SetX(mario.GetX() + (marioVel.x * dTime));
	
	if (currInput->GetKeyDown(VK_DOWN) || currInput->GetControllerDown(1, XINPUT_GAMEPAD_DPAD_DOWN))
	{
		mario.SetAnimation(DUCK);
	}
}

void Scene::Render(float dTime)
{
	mario.Animate(dTime);
	mario.DrawFrame();

	// push to front buffer
	DisplayFrame();
}

void Scene::Resize(HWND* newHandler)
{
    hwnd = newHandler; 

	SetBuffers();
}

void Scene::Shutdown()
{
    SelectObject(backHDC,theOldFrontBitMap);
    DeleteDC(backHDC);
    ReleaseDC(*hwnd,frontHDC);
}
#include "Vector2D.h"

Vector2D::Vector2D()
{
	x = 0;
	y = 0;
}

Vector2D::Vector2D(float initialX, float initialY)
{
	x = initialX;
	y = initialY;
}

float Vector2D::Magnitude()
{
	float magnitude = sqrt(pow(x, 2) + pow(y, 2));

	return magnitude;
}

void Vector2D::Add(Vector2D newVector)
{
	x += newVector.x;
	y += newVector.y;
}


float Vector2D::Trajectory()
{
	float pi = atan(1.0)*4;

	float angle = (-1) * (atan2(x, y) - pi) * (360 / (pi * 2));

	return angle;
}

void Vector2D::Set(float newX, float newY)
{
	x = newX;
	y = newY;
}

void Vector2D::Set(Vector2D value)
{
	x = value.x;
	y = value.y;
}
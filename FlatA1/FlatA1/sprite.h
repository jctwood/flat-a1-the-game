#ifndef SPRITE_H
#define SPRITE_H

#include <Windows.h>
#include <MMSystem.h>
#include <stdio.h>

class Sprite
{
public:
	Sprite();
	void Initialise(LPSTR filename, int xPos, int yPos, HDC *back, HDC *bitmap, HDC *flippedBitmap);
	void SetX(int);
	void SetY(int);
	void SetWidth(int);
	void SetHeight(int);
	int GetX();
	int GetY();
	int GetHeight();
	int GetWidth();
	HBITMAP GetBitmap();
	void Flip();
	bool GetFlipped();
	void DrawSprite();

protected:
	int x, y, width, height;
	bool flipped;
	HBITMAP bitmap;
	HDC *bitmapHDC, *backHDC, *flippedHDC;
	void LoadBitmap(LPSTR);
};

#endif
